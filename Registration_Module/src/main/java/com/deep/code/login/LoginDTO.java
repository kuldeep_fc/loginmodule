package com.deep.code.login;
/*
 * Login Entity
 * 
 * Author :- Kuldeep Argal 29/07/2020
 * 
 * */

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="auth")
public class LoginDTO 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;
  private String email;
  private String password;
  private String browserName;
  private String ipAddress;
  private LocalDateTime loginTimeDate;
  private String systemName;
  
  
public String getBrowserName() {
	return browserName;
}
public void setBrowserName(String browserName) {
	this.browserName = browserName;
}
public String getIpAddress() {
	return ipAddress;
}
public void setIpAddress(String ipAddress) {
	this.ipAddress = ipAddress;
}


public String getSystemName() {
	return systemName;
}
public void setSystemName(String systemName) {
	this.systemName = systemName;
}
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}

public LoginDTO() {
	super();
	// TODO Auto-generated constructor stub
}
public LocalDateTime getLoginTimeDate() {
	return loginTimeDate;
}
public void setLoginTimeDate(LocalDateTime loginTimeDate) {
	this.loginTimeDate = loginTimeDate;
}
public LoginDTO(Long id, String email, String password, String browserName, String ipAddress,
		LocalDateTime loginTimeDate, String systemName) {
	super();
	this.id = id;
	this.email = email;
	this.password = password;
	this.browserName = browserName;
	this.ipAddress = ipAddress;
	this.loginTimeDate = loginTimeDate;
	this.systemName = systemName;
}

  
}
