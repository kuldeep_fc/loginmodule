package com.deep.code.login;

import java.net.InetAddress;
import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.deep.code.BaseResponse;

/*
 * Login Controller
 * 
 * Author :- Kuldeep Argal 29/07/2020
 * 
 * */

@RestController
@RequestMapping("/api/auth")
public class LoginController 
{  
	
    @Autowired
    private LoginRepository loginRepository;
    
    @Autowired
    HttpServletRequest req;
    
    /*
     * Registration Met 
     * */
	
    /*
     * Login Method
     * */
	
    @PostMapping("/login")
	public BaseResponse loginAuthentication(@RequestBody  LoginDTO login)throws Exception 
	{
		BaseResponse response=new BaseResponse(true);
		LoginDTO loginDTO=loginRepository.loginByEmail(login.getEmail());
		
		if(loginDTO==null)
		{
			   response.setSuccess(false);
			   response.addMessage(BaseResponse.FAILED);
			   return response;
		}
		else  
		{
			if(login.getEmail().equals(loginDTO.getEmail()))
			{
				    LoginDTO dto=new LoginDTO();
				    dto.setPassword(login.getPassword());
					dto.setEmail(login.getEmail());
					dto.setId(loginDTO.getId());
					// For Ip Address
				    InetAddress localhost = InetAddress.getLocalHost();
					dto.setIpAddress(localhost.getHostAddress().trim());
					//Brower Details
					dto.setBrowserName(req.getHeader("user-agent"));
					//For System Name
					dto.setSystemName(localhost.getHostName().trim());
					//Current Date
					LocalDateTime current = LocalDateTime.now();
					dto.setLoginTimeDate(current);
					loginRepository.save(dto);
					response.setSuccess(true);
					response.addMessage(BaseResponse.SUCCESS);
					
					return response;
				
				
			 }
			    response.setSuccess(false);
			    response.addMessage(BaseResponse.FAILED);
			   return response;
		}
		
		
	}
	 
}
